local _, dap = pcall(require, "dap")
if dap then
	-- dap.configurations.c
	local dap_configs = {
		{
			name = "Hashtable unittest debug",
			type = "cppdbg",
			request = "launch",
			program = vim.fn.getcwd() .. "/build/unittest",
			cwd = vim.fn.getcwd(),
			stopAtEntry = true,
		},
	}

	for _, c in pairs(dap_configs) do
		local exist = false
		for _, conf in pairs(dap.configurations.c) do
			if conf.name == c.name then
				exist = true
				break
			end
		end
		if not exist then
			table.insert(dap.configurations.c, c)
		end
	end
end
local _, overseer = pcall(require, "overseer")
if overseer then
	overseer.register_template({
		name = "Hashtable Build ( debug )",
		builder = function()
			return {
				cmd = "mkdir build; cd build && cmake -DCMAKE_BUILD_TYPE=Debug .. && make",
			}
		end,
	})
	overseer.register_template({
		name = "Hashtable Build",
		builder = function()
			return {
				cmd = "mkdir build; cd build && cmake .. && make",
			}
		end,
	})
	overseer.register_template({
		name = "Hashtable Run unittest",
		builder = function ()
			return {
				cmd = "./build/unittest"
			}
		end,
	})
end
