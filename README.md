# NOTE

This code copied from https://github.com/a6guerre/hash-table-library-c

## Build

```console
mkdir build
cd build
cmake ..
make
```

## Build for debugging

```console
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```
